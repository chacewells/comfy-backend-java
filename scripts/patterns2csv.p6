#!/usr/bin/env perl6
use JSON::Fast;
use Text::CSV;

my constant $PATTERNS-FILE = 'src/main/resources/reference-data/patterns.json';
my constant $OUTFILE = 'data/patterns.csv';

my $patterns = $PATTERNS-FILE.IO.slurp.&from-json;
my $csv = Text::CSV.new: binary => True, auto_diag => True, eol => "\n";
my $out = open $OUTFILE, :w;

my @header;
for @$patterns -> %pattern {
    for %pattern.keys -> $key {
        push @header, $key unless $key ∈ set(@header);
    }
}

$csv.say: $out, @header;

for @$patterns -> %pattern {
    my @record = map -> $h { %pattern{$h} orelse '' }, @header;
    $csv.say: $out, @record;
}

$out.close;
