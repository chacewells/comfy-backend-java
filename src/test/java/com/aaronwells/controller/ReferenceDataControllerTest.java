package com.aaronwells.controller;

import com.aaronwells.model.Color;
import com.aaronwells.model.Instrument;
import com.aaronwells.model.Pattern;
import com.aaronwells.service.ReferenceDataService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class ReferenceDataControllerTest {

    @Mock
    ReferenceDataService referenceDataService;

    @InjectMocks
    ReferenceDataController controller;

    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void getColors() throws Exception {
        List<Color> colors = new ArrayList<>();
        when(referenceDataService.getColors()).thenReturn(colors);

        mockMvc.perform(get("/api/colors"))
                .andExpect(status().isOk());

        verify(referenceDataService).getColors();
    }

    @Test
    void getInstruments() throws Exception {
        List<Instrument> instruments = new ArrayList<>();
        when(referenceDataService.getInstruments()).thenReturn(instruments);

        mockMvc.perform(get("/api/instruments"))
                .andExpect(status().isOk());

        verify(referenceDataService).getInstruments();
    }

    @Test
    void getPatterns() throws Exception {
        var patterns = new ArrayList<Pattern>();
        when(referenceDataService.getPatterns()).thenReturn(patterns);

        mockMvc.perform(get("/api/patterns"))
                .andExpect(status().isOk());

        verify(referenceDataService).getPatterns();
    }
}