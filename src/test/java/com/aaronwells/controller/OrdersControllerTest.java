package com.aaronwells.controller;

import com.aaronwells.model.*;
import com.aaronwells.service.orders.OrdersSheetService;
import com.aaronwells.service.paypal.PaypalService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class OrdersControllerTest {

    ObjectMapper objectMapper;

    @Mock
    PaypalService paypalService;

    @Mock
    OrdersSheetService ordersSheetService;

    @InjectMocks
    OrdersController controller;

    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
        mockMvc = MockMvcBuilders
                .standaloneSetup(controller)
//                .setMessageConverters(messageConverter)
                .build();
    }

    @Test
    void recordPaypalTransaction() throws Exception {
        var paypalId = "id";
        var order = objectMapper.writeValueAsString(
                Order.builder()
                        .paypalId(paypalId)
                        .cart(Arrays.asList(Strap.builder()
                                .id(UUID.randomUUID())
                                .embroideredColor(color())
                                .pattern(pattern())
                                .instrument(instrument())
                                .build()
                                )
                        )
                .build()
        );
        var paypalDetails = PaypalDetails.builder().paypalId(paypalId).build();
        var orderSheetRow = new ArrayList<List<Object>>();
        when(paypalService.fetchDetails(anyString())).thenReturn(paypalDetails);
        when(ordersSheetService.saveOrderDetails(any(), any())).thenReturn(new AsyncResult<>(orderSheetRow));

        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(order))
                .andExpect(status().isOk());

        verify(paypalService).fetchDetails(any());
        verify(ordersSheetService).saveOrderDetails(any(), any());
    }

    @Test
    void recordPaypalTransaction_notFound() throws Exception {
        var paypalId = "any";
        var order = objectMapper.writeValueAsString(
                Order.builder().paypalId(paypalId).build()
        );
        when(paypalService.fetchDetails(anyString())).thenReturn(null);

        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(order))
                .andExpect(status().isOk());

        verify(paypalService).fetchDetails(any());
        verifyNoMoreInteractions(ordersSheetService);
    }

    Instrument instrument() {
        return Instrument.builder().id(UUID.randomUUID()).name("instrument").build();
    }

    Color color() {
        return Color.builder().id(UUID.randomUUID()).name("color").build();
    }

    Pattern pattern() {
        return Pattern.builder().id(UUID.randomUUID()).name("pattern").build();
    }

}