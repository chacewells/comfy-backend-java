package com.aaronwells.service.orders;

import com.aaronwells.config.OrderHistorySpreadsheetConfig;
import com.aaronwells.converter.DenormalizedOrderItemToRow;
import com.aaronwells.model.Order;
import com.aaronwells.model.PaypalDetails;
import com.aaronwells.model.Strap;
import com.google.api.services.sheets.v4.Sheets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.time.temporal.ValueRange;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrdersSheetServiceTest {

    @Mock
    Sheets sheets;
    @Mock
    Sheets.Spreadsheets spreadsheets;
    @Mock
    Sheets.Spreadsheets.Values values;
    @Mock
    Sheets.Spreadsheets.Values.Append append;

    DenormalizedOrderItemToRow itemToRowConverter;

    final String readWriteSpreadsheetId = "spreadsheetid";
    final String readWriteSpreadsheetRange = "spreadsheetrange";

    OrdersSheetService service;

    @BeforeEach
    void setUp() {
        var spreadsheetConfig = new OrderHistorySpreadsheetConfig(readWriteSpreadsheetId, readWriteSpreadsheetRange);
        itemToRowConverter = new DenormalizedOrderItemToRow();
        service = new OrdersSheetService(sheets, itemToRowConverter, spreadsheetConfig);
    }

    @Test
    void saveOrderDetails() throws IOException, ExecutionException, InterruptedException {
        var paypalDetails = PaypalDetails.builder().paypalId("id").build();
        var order = Order.builder()
                .id(UUID.randomUUID())
                .paypalId(paypalDetails.getPaypalId())
                .cart(
                        Arrays.asList(
                                Strap.builder().id(UUID.randomUUID()).build()))
                .build();
        when(sheets.spreadsheets()).thenReturn(spreadsheets);
        when(spreadsheets.values()).thenReturn(values);
        when(values.append(anyString(), anyString(), any())).thenReturn(append);
        when(append.setValueInputOption(anyString())).thenReturn(append);

        var result = service.saveOrderDetails(paypalDetails, order);

        verify(append).execute();
        assertThat(result.get().get(0).get(0)).isEqualTo(paypalDetails.getPaypalId());
    }

}