package com.aaronwells.service;

import com.aaronwells.model.PaypalDetails;
import com.aaronwells.service.paypal.PaypalAuthenticator;
import com.aaronwells.service.paypal.PaypalService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;

import static com.aaronwells.service.paypal.PaypalService.PATH_PAYPAL_ID;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration(classes = PaypalTestConfig.class)
class PaypalServiceTest {

    static final String RESPONSE_BODY_RESOURCE = "paypal-response.json";

    @Mock
    PaypalAuthenticator paypalAuthenticator;

    @Mock
    RestTemplate paypalRestTemplate;

    @Mock
    ResponseEntity<JsonNode> response;

    @InjectMocks
    PaypalService paypalService;

    @BeforeEach
    void setup() {
        when(paypalAuthenticator.getAuthToken()).thenReturn("token");
    }

    @Test
    void fetchDetails() {
        var paypalId = "orderId";
        when(response.getBody()).thenReturn(loadJsonBody(RESPONSE_BODY_RESOURCE));
        when(response.getStatusCode()).thenReturn(OK);
        when(paypalRestTemplate.exchange(
                eq(PATH_PAYPAL_ID),
                eq(GET),
                any(HttpEntity.class),
                eq(JsonNode.class),
                eq(paypalId)))
                .thenReturn(response);

        var expectedDetails = PaypalDetails.builder()
                .paypalId("3K343737B22928401")
                .addressLine1("1 Main St")
                .city("San Jose")
                .state("CA")
                .country("US")
                .postalCode("95131")
                .fullName("John Doe")
                .build();

        var paypalDetails = paypalService.fetchDetails(paypalId);
        assertThat(paypalDetails).isEqualTo(expectedDetails);
    }

    @Test
    void fetchDetails_notFound_shouldReturnNull() {
        var paypalId = "orderId";
        when(response.getStatusCode()).thenReturn(NOT_FOUND);
        when(paypalRestTemplate.exchange(
                eq(PATH_PAYPAL_ID),
                eq(GET),
                any(HttpEntity.class),
                eq(JsonNode.class),
                eq(paypalId)))
                .thenReturn(response);

        var paypalDetails = paypalService.fetchDetails(paypalId);
        assertThat(paypalDetails).isNull();
    }

    private JsonNode loadJsonBody(String resourceName) {
        try (
                InputStream inputStream = getClass().getClassLoader().getResourceAsStream(resourceName)
        ) {
            var result = new ObjectMapper().readTree(inputStream);
            return result;
        } catch (IOException e) {
            throw new RuntimeException(format("Failed to load json resource file %s", resourceName), e);
        }
    }

}
