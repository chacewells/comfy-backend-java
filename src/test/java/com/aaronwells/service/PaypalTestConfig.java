package com.aaronwells.service;

import com.aaronwells.service.paypal.PaypalService;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = PaypalService.class)
public class PaypalTestConfig {
}
