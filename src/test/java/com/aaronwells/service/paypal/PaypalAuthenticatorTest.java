package com.aaronwells.service.paypal;

import com.aaronwells.config.PaypalAuthConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PaypalAuthenticatorTest {

    @Mock
    RestTemplate paypalRestTemplate;

    PaypalAuthConfig paypalAuthConfig;

    PaypalAuthenticator paypalAuthenticator;

    @BeforeEach
    void setUp() {
        paypalAuthConfig = new PaypalAuthConfig("client", "secret");
        paypalAuthenticator = new PaypalAuthenticator(paypalRestTemplate, paypalAuthConfig);
    }

    @Test
    void getAuthToken() {
        ResponseEntity<PaypalAuthResponse> response = mock(ResponseEntity.class);
        var authBody = new PaypalAuthResponse("token", 20L);
        when(response.getBody()).thenReturn(authBody);
        when(paypalRestTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class)))
            .thenReturn(response);

        String tokenFirstCall = paypalAuthenticator.getAuthToken();
        String tokenSecondCall = paypalAuthenticator.getAuthToken();
        assertThat(tokenFirstCall).isEqualTo(authBody.getAccessToken());
        assertThat(tokenSecondCall).isEqualTo(tokenFirstCall);
        verify(paypalRestTemplate, times(1))
                .postForEntity(anyString(), any(HttpEntity.class), eq(PaypalAuthResponse.class));

    }
}