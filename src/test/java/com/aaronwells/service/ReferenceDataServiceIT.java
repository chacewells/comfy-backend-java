package com.aaronwells.service;

import com.aaronwells.config.ReferenceDataConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ContextConfiguration(classes = ReferenceDataConfig.class)
@ExtendWith(SpringExtension.class)
class ReferenceDataServiceIT {

    @Autowired
    ReferenceDataService service;

    @Test
    void getColors() {
        assertNotNull(service.getColors());
    }

    @Test
    void getInstruments() {
        assertNotNull(service.getInstruments());
    }

    @Test
    void getPatterns() {
        assertNotNull(service.getPatterns());
    }
}