package com.aaronwells.script;

import com.aaronwells.Application;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.UUID;

@Slf4j
public class RewriteRefDataWithID {
    final ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    final ClassLoader classLoader = Application.class.getClassLoader();

    @Test
    @Disabled
    void addIds() throws IOException, URISyntaxException {
        var models = Arrays.asList("colors", "instruments", "patterns");

        for (String model: models) {
            ArrayNode data;
            String inPath = "reference-data/" + model + ".json";
            Path inPathResource = Paths.get("src/main/resources", inPath);

            try (InputStream in = classLoader.getResourceAsStream(inPath)) {
                data = objectMapper.readValue(in, ArrayNode.class);
                log.debug("Got data for {}", inPath);
            }

            ArrayNode withIds = objectMapper.createArrayNode();
            for (JsonNode item: data) {
                ObjectNode newItem = objectMapper.treeToValue(item, ObjectNode.class);
                newItem.put("_id", UUID.randomUUID().toString());
                withIds.add(newItem);
            }

            Path outPath = inPathResource.getParent().resolve(model + "-with-id.json");
            log.debug("Writing to {}", outPath);
            try (OutputStream out = new FileOutputStream(outPath.toFile())) {
                objectMapper.writeValue(out, withIds);
            }
        }
    }
}



