package com.aaronwells.converter;

import com.aaronwells.model.DenormalizedOrderItem;
import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DenormalizedOrderItemToRowTest {

    final String paypalId = "paypalid";
    final String customerName = "customer full name";
    final String addressLine1 = "address line 1";
    final String addressLine2 = "address line 2";
    final String city = "city";
    final String state = "state";
    final String country = "country";
    final String postal = "postal";
    final String pattern = "pattern";
    final String instrument = "instrument";
    final Boolean embroidered = Boolean.TRUE;
    final String color = "color";
    final String text = "text";
    final String notes = "notes";
    final String quantity = "quantity";

    DenormalizedOrderItemToRow converter;

    @BeforeEach
    void setUp() {
        converter = new DenormalizedOrderItemToRow();
    }

    @Test
    void convert() {
        var from = DenormalizedOrderItem.builder()
                .paypalId(paypalId)
                .customerFullName(customerName)
                .customerAddressLine1(addressLine1)
                .customerAddressLine2(addressLine2)
                .customerCity(city)
                .customerState(state)
                .customerCountry(country)
                .customerPostalCode(postal)
                .patternName(pattern)
                .instrumentName(instrument)
                .embroidered(embroidered)
                .embroideredColorName(color)
                .embroideredText(text)
                .quantity(quantity)
                .notes(notes)
                .build();

        var expected = Arrays.asList(new Object[] {
                paypalId,
                customerName,
                addressLine1,
                addressLine2,
                city,
                state,
                country,
                postal,
                pattern,
                instrument,
                String.valueOf(embroidered),
                color,
                text,
                quantity,
                notes});

        var to = converter.convert(from);
        assertThat(to).isEqualTo(expected);
    }

}