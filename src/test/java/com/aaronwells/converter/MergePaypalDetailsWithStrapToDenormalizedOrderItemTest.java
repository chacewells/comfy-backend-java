package com.aaronwells.converter;

import com.aaronwells.model.*;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MergePaypalDetailsWithStrapToDenormalizedOrderItemTest {

    @Test
    void convert() {
        // given
        PaypalDetails paypalDetails = paypalDetailsBuilder().build();
        Strap strap = strapBuilder().build();
        MergePaypalDetailsWithStrapToDenormalizedOrderItem merger =
                new MergePaypalDetailsWithStrapToDenormalizedOrderItem(paypalDetails);

        // when
        DenormalizedOrderItem item = merger.convert(strap);

        // then
        assertEquals(paypalDetails.getPaypalId(), item.getPaypalId());
        assertEquals(paypalDetails.getAddressLine1(), item.getCustomerAddressLine1());
        assertEquals(paypalDetails.getAddressLine2(), item.getCustomerAddressLine2());
        assertEquals(paypalDetails.getCity(), item.getCustomerCity());
        assertEquals(paypalDetails.getState(), item.getCustomerState());
        assertEquals(paypalDetails.getCountry(), item.getCustomerCountry());
        assertEquals(paypalDetails.getPostalCode(), item.getCustomerPostalCode());
        assertEquals(paypalDetails.getFullName(), item.getCustomerFullName());

        assertEquals(strap.getPattern().getName(), item.getPatternName());
        assertEquals(strap.getInstrument().getName(), item.getInstrumentName());
        assertEquals(strap.getEmbroidered(), item.getEmbroidered());
        assertEquals(strap.getEmbroideredColor().getName(), item.getEmbroideredColorName());
        assertEquals(strap.getEmbroideredText(), item.getEmbroideredText());
        assertEquals(strap.getNotes(), item.getNotes());
        assertEquals(String.valueOf(strap.getQuantity()), item.getQuantity());
    }

    @Test
    void convertNoPattern() {
        // given
        PaypalDetails paypalDetails = paypalDetailsBuilder().build();
        Strap strap = strapBuilder().pattern(null).build();
        MergePaypalDetailsWithStrapToDenormalizedOrderItem merger =
                new MergePaypalDetailsWithStrapToDenormalizedOrderItem(paypalDetails);

        // when
        DenormalizedOrderItem item = merger.convert(strap);

        // then
        assertEquals("", item.getPatternName());
    }

    @Test
    void convertNoInstrument() {
        // given
        PaypalDetails paypalDetails = paypalDetailsBuilder().build();
        Strap strap = strapBuilder().instrument(null).build();
        MergePaypalDetailsWithStrapToDenormalizedOrderItem merger =
                new MergePaypalDetailsWithStrapToDenormalizedOrderItem(paypalDetails);

        // when
        DenormalizedOrderItem item = merger.convert(strap);

        // then
        assertEquals("", item.getInstrumentName());
    }

    @Test
    void convertNoColor() {
        // given
        PaypalDetails paypalDetails = paypalDetailsBuilder().build();
        Strap strap = strapBuilder().embroideredColor(null).build();
        MergePaypalDetailsWithStrapToDenormalizedOrderItem merger =
                new MergePaypalDetailsWithStrapToDenormalizedOrderItem(paypalDetails);

        // when
        DenormalizedOrderItem item = merger.convert(strap);

        // then
        assertEquals("", item.getEmbroideredColorName());
    }

    @Test
    void convert_nullStrap_blowsUp() {
        // given
        PaypalDetails paypalDetails = paypalDetailsBuilder().build();
        Strap strap = null;
        MergePaypalDetailsWithStrapToDenormalizedOrderItem merger =
                new MergePaypalDetailsWithStrapToDenormalizedOrderItem(paypalDetails);

        // when
        Throwable throwable = catchThrowable(() -> merger.convert(strap));

        // then
        assertThat(throwable).isInstanceOf(NullPointerException.class);
    }

    @Test
    void convert_nullPaypal_blowsUp() {
        // given
        PaypalDetails paypalDetails = null;
        Strap strap = strapBuilder().build();
        MergePaypalDetailsWithStrapToDenormalizedOrderItem merger =
                new MergePaypalDetailsWithStrapToDenormalizedOrderItem(paypalDetails);

        // when
        Throwable throwable = catchThrowable(() -> merger.convert(strap));

        // then
        assertThat(throwable).isInstanceOf(NullPointerException.class);
    }

    PaypalDetails.PaypalDetailsBuilder paypalDetailsBuilder() {
        return PaypalDetails.builder()
                .paypalId("paypalid")
                .addressLine1("line 1")
                .addressLine2("line 2")
                .city("city")
                .state("ST")
                .country("US")
                .postalCode("postl")
                .fullName("Full N. Ame");
    }

    Strap.StrapBuilder strapBuilder() {
        return Strap.builder()
                .id(UUID.randomUUID())
                .pattern(
                        Pattern.builder()
                                .id(UUID.randomUUID())
                                .name("pattern")
                                .image("pattern.jpg")
                                .price(new BigDecimal(30.00))
                                .canEmbroider(true)
                                .build()
                )
                .instrument(
                        Instrument.builder()
                                .id(UUID.randomUUID())
                                .name("instrument")
                                .build()
                )
                .embroideredColor(
                        Color.builder()
                                .id(UUID.randomUUID())
                                .name("color")
                                .build()
                )
                .embroideredText("embroider me")
                .notes("notes")
                .quantity(1);
    }
}