package com.aaronwells.service.orders;

import com.aaronwells.config.OrderHistorySpreadsheetConfig;
import com.aaronwells.converter.DenormalizedOrderItemToRow;
import com.aaronwells.converter.MergePaypalDetailsWithStrapToDenormalizedOrderItem;
import com.aaronwells.model.*;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrdersSheetService {

    private final Sheets sheets;
    private final DenormalizedOrderItemToRow itemToRowConverter;

    private final String readWriteSpreadsheetId;
    private final String readWriteSpreadsheetRange;

    public OrdersSheetService(
            Sheets sheetsService,
            DenormalizedOrderItemToRow itemToRowConverter,
            OrderHistorySpreadsheetConfig orderHistorySpreadsheetConfig) {
        this.sheets = sheetsService;
        this.itemToRowConverter = itemToRowConverter;
        this.readWriteSpreadsheetId = orderHistorySpreadsheetConfig.getSpreadsheetId();
        this.readWriteSpreadsheetRange = orderHistorySpreadsheetConfig.getSpreadsheetRange();
    }

//    @Async
    public Future<List<List<Object>>> saveOrderDetails(PaypalDetails paypalDetails, Order order) throws IOException {
        var paypalDetailsMerger =
                new MergePaypalDetailsWithStrapToDenormalizedOrderItem(paypalDetails);

        List<List<Object>> sheetsOrderList = order
                .getCart().stream()
                .map(paypalDetailsMerger::convert)
                .filter(Objects::nonNull)
                .map(itemToRowConverter::convert)
                .collect(Collectors.toList());

        var appendRequest = new ValueRange().setValues(sheetsOrderList);

        log.debug("Sending sheets request");
        sheets.spreadsheets().values()
                .append(readWriteSpreadsheetId, readWriteSpreadsheetRange, appendRequest)
                .setValueInputOption("USER_ENTERED")
                .execute();
        log.debug("Sheets request sent");

        return new AsyncResult<>(sheetsOrderList);
    }

}
