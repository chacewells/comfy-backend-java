package com.aaronwells.service;

import com.aaronwells.model.Color;
import com.aaronwells.model.Instrument;
import com.aaronwells.model.Pattern;
import lombok.Builder;
import lombok.Getter;

import java.util.LinkedList;
import java.util.List;

@Getter
public class ReferenceDataService {
    private List<Color> colors = new LinkedList<>();
    private List<Instrument> instruments = new LinkedList<>();
    private List<Pattern> patterns = new LinkedList<>();

    @Builder
    public ReferenceDataService(List<Color> colors, List<Instrument> instruments, List<Pattern> patterns) {
        if (colors != null)
            this.colors = colors;
        if (instruments != null)
            this.instruments = instruments;
        if (patterns != null)
            this.patterns = patterns;
    }
}
