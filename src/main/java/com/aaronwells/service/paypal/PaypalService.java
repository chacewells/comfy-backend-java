package com.aaronwells.service.paypal;

import com.aaronwells.model.PaypalDetails;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PaypalService {

    private static final Logger LOG = LoggerFactory.getLogger(PaypalService.class);

    public static final String PATH_PAYPAL_ID = "/v2/checkout/orders/{paypalId}";

    private final PaypalAuthenticator paypalAuthenticator;

    private final RestTemplate paypalRestTemplate;

    public PaypalService(PaypalAuthenticator paypalAuthenticator, RestTemplate paypalRestTemplate) {
        this.paypalAuthenticator = paypalAuthenticator;
        this.paypalRestTemplate = paypalRestTemplate;
    }

    public PaypalDetails fetchDetails(String paypalId) {
        LOG.debug("Fetching details for paypal id: {}", paypalId);
        String authToken = paypalAuthenticator.getAuthToken();

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(authToken);
        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

        ResponseEntity<JsonNode> response = paypalRestTemplate.exchange(
            PATH_PAYPAL_ID,
            HttpMethod.GET,
            requestEntity,
            JsonNode.class,
            paypalId
        );

        LOG.debug("PayPal responded: {}", response.getStatusCode());
        PaypalDetails paypalDetails = null;
        if (response.getStatusCode().is2xxSuccessful()) {
            LOG.debug("paypalResponse: {}", response.getBody());
            paypalDetails = extractPaypalDetails(response.getBody());
        }

        return paypalDetails;
    }

    private static PaypalDetails extractPaypalDetails(JsonNode response) {
        JsonNode purchaseUnits = response.at("/purchase_units");
        JsonNode shipping = purchaseUnits.at("/0/shipping");
        var paypalDetails = PaypalDetails.builder()
                .paypalId(response.at("/id").asText())
                .addressLine1(shipping.at("/address/address_line_1").asText(null))
                .addressLine2(shipping.at("/address/address_line_2").asText(null))
                .state(shipping.at("/address/admin_area_1").asText(null))
                .city(shipping.at("/address/admin_area_2").asText(null))
                .country(shipping.at("/address/country_code").asText(null))
                .postalCode(shipping.at("/address/postal_code").asText(null))
                .fullName(shipping.at("/name/full_name").asText(null))
                .build();

        return paypalDetails;
    }

}
