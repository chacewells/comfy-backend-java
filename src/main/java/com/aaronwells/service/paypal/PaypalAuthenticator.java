package com.aaronwells.service.paypal;

import com.aaronwells.config.PaypalAuthConfig;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import static java.util.Arrays.asList;

@Component
public class PaypalAuthenticator {

    private static final int MILLIS_PER_SECOND = 1000;

    public static final String AUTH_TOKEN_PATH = "/v1/oauth2/token/";

    private final String clientId;
    private final String clientSecret;

    private final RestTemplate paypalRestTemplate;

    private String token;
    private Long expiryTimeMillis;

    public PaypalAuthenticator(RestTemplate paypalRestTemplate, PaypalAuthConfig paypalAuthConfig) {
        this.paypalRestTemplate = paypalRestTemplate;
        clientId = paypalAuthConfig.getClientId();
        clientSecret = paypalAuthConfig.getClientSecret();
    }

    private void authenticate() {
        var headers = new HttpHeaders();
        headers.setBasicAuth(clientId, clientSecret);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(asList(MediaType.APPLICATION_JSON));
        var entity = new HttpEntity<>("grant_type=client_credentials", headers);

        var response = paypalRestTemplate.postForEntity(AUTH_TOKEN_PATH, entity, PaypalAuthResponse.class);
        var authInfo = response.getBody();

        setToken(authInfo);
        setExpiry(authInfo);
    }

    private void setExpiry(PaypalAuthResponse authInfo) {
        var expiresInMillis = authInfo.getExpiresIn() * MILLIS_PER_SECOND;
        expiryTimeMillis = System.currentTimeMillis() + expiresInMillis;
    }

    private void setToken(PaypalAuthResponse authInfo) {
        this.token = authInfo.getAccessToken();
    }

    private boolean isTokenExpired() {
        return token == null
                || expiryTimeMillis == null
                || expiryTimeMillis <= System.currentTimeMillis();
    }

    public String getAuthToken() {
        if (isTokenExpired())
            authenticate();
        return token;
    }

}
