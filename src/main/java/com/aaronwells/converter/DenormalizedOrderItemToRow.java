package com.aaronwells.converter;

import com.aaronwells.model.DenormalizedOrderItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

@Slf4j
@Component
public class DenormalizedOrderItemToRow implements Converter<DenormalizedOrderItem, List<Object>> {
    @Override
    public List<Object> convert(DenormalizedOrderItem denormalizedOrderItem) {
        List<Object> converted =  Arrays.asList(
                defaultToEmpty(denormalizedOrderItem.getPaypalId()),
                defaultToEmpty(denormalizedOrderItem.getCustomerFullName()),
                defaultToEmpty(denormalizedOrderItem.getCustomerAddressLine1()),
                defaultToEmpty(denormalizedOrderItem.getCustomerAddressLine2()),
                defaultToEmpty(denormalizedOrderItem.getCustomerCity()),
                defaultToEmpty(denormalizedOrderItem.getCustomerState()),
                defaultToEmpty(denormalizedOrderItem.getCustomerCountry()),
                defaultToEmpty(denormalizedOrderItem.getCustomerPostalCode()),
                defaultToEmpty(denormalizedOrderItem.getPatternName()),
                defaultToEmpty(denormalizedOrderItem.getInstrumentName()),
                defaultToEmpty(String.valueOf(denormalizedOrderItem.getEmbroidered())),
                defaultToEmpty(denormalizedOrderItem.getEmbroideredColorName()),
                defaultToEmpty(denormalizedOrderItem.getEmbroideredText()),
                defaultToEmpty(denormalizedOrderItem.getQuantity()),
                defaultToEmpty(denormalizedOrderItem.getNotes())
        );
        log.debug("order row: {}", converted);
        return converted;
    }

    private static String defaultToEmpty(String input) {
        return defaultIfBlank(input, "");
    }
}
