package com.aaronwells.converter;

import com.aaronwells.model.*;
import org.springframework.core.convert.converter.Converter;

import java.util.Optional;

public class MergePaypalDetailsWithStrapToDenormalizedOrderItem implements Converter<Strap, DenormalizedOrderItem> {

    private final PaypalDetails paypalDetails;

    public MergePaypalDetailsWithStrapToDenormalizedOrderItem(PaypalDetails paypalDetails) {
        this.paypalDetails = paypalDetails;
    }

    @Override
    public DenormalizedOrderItem convert(Strap strap) {
        return DenormalizedOrderItem.builder()
                .paypalId(paypalDetails.getPaypalId())
                .customerFullName(paypalDetails.getFullName())
                .customerAddressLine1(paypalDetails.getAddressLine1())
                .customerAddressLine2(paypalDetails.getAddressLine2())
                .customerCity(paypalDetails.getCity())
                .customerState(paypalDetails.getState())
                .customerCountry(paypalDetails.getCountry())
                .customerPostalCode(paypalDetails.getPostalCode())
                .patternName(
                        Optional.of(strap)
                                .map(Strap::getPattern)
                                .map(Pattern::getName)
                                .orElse(""))
                .notes(strap.getNotes())
                .quantity(String.valueOf(strap.getQuantity()))
                .instrumentName(
                        Optional.of(strap)
                                .map(Strap::getInstrument)
                                .map(Instrument::getName)
                                .orElse(""))
                .embroidered(strap.getEmbroidered())
                .embroideredText(strap.getEmbroideredText())
                .embroideredColorName(
                        Optional.of(strap)
                                .map(Strap::getEmbroideredColor)
                                .map(Color::getName)
                                .orElse("")
                )
                .build();
    }

}
