package com.aaronwells.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class OrderHistorySpreadsheetConfig {
    private final String spreadsheetId;
    private final String spreadsheetRange;

    public OrderHistorySpreadsheetConfig(
            @Value("${sheets.order.spreadsheet.id}") String spreadsheetId,
            @Value("${sheets.order.spreadsheet.range}") String spreadsheetRange) {
        this.spreadsheetId = spreadsheetId;
        this.spreadsheetRange = spreadsheetRange;
    }
}
