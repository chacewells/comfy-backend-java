package com.aaronwells.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class PaypalAuthConfig {
    private final String clientId;
    private final String clientSecret;

    public PaypalAuthConfig(
            @Value("${paypal.client.id}") String clientId,
            @Value("${paypal.client.secret}") String clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }
}

