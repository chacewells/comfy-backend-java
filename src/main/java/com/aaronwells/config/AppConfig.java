package com.aaronwells.config;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import io.sentry.spring.SentryExceptionResolver;
import io.sentry.spring.SentryServletContextInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerExceptionResolver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableAsync
public class AppConfig {

    @Bean
    public JacksonFactory jacksonFactory() {
        return JacksonFactory.getDefaultInstance();
    }

    @Bean
    public DataStoreFactory dataStoreFactory(@Value("${sheets.tokens.directory.path}") String tokensDirectoryPath)
    throws IOException {
        return new FileDataStoreFactory(new File(tokensDirectoryPath));
    }

    @Bean
    public NetHttpTransport netHttpTransport() throws IOException, GeneralSecurityException {
        return GoogleNetHttpTransport.newTrustedTransport();
    }

    /*
    Sentry integration
     */
    @Bean
    public HandlerExceptionResolver sentryExceptionResolver() {
        return new SentryExceptionResolver();
    }

    @Bean
    public ServletContextInitializer sentryServletContextInitializer() {
        return new SentryServletContextInitializer();
    }

    @Bean
    public Credential sheetsCredential(
            @Value("${sheets.credentials.location}") Resource credentialsResource
    ) throws IOException {
        try (InputStream in = credentialsResource.getInputStream()) {
            return GoogleCredential.fromStream(in)
                    .createScoped(sheetsScopes());
        }
    }

    @Bean
    public Sheets sheetsService(
            @Autowired Credential sheetsCredential,
            @Autowired JacksonFactory jacksonFactory,
            @Autowired NetHttpTransport netHttpTransport,
            @Value("${sheets.application.name}") String applicationName
    ) {
        return new Sheets.Builder(netHttpTransport, jacksonFactory, sheetsCredential)
                .setApplicationName(applicationName)
                .build();
    }

    @Bean
    public RestTemplate paypalRestTemplate(
            RestTemplateBuilder restTemplateBuilder,
            @Value("${paypal.host.uri}") String paypalOrdersUri) {
        return restTemplateBuilder
                .rootUri(paypalOrdersUri)
                .build();
    }

    private List<String> sheetsScopes() {
        return Collections.singletonList(SheetsScopes.SPREADSHEETS);
    }

}
