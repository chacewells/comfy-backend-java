package com.aaronwells.config;

import com.aaronwells.model.Color;
import com.aaronwells.model.Instrument;
import com.aaronwells.model.Pattern;
import com.aaronwells.service.ReferenceDataService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Configuration
public class ReferenceDataConfig {
    @Value("classpath:/reference-data/colors.json")
    private Resource colorsResource;

    @Value("classpath:/reference-data/instruments.json")
    private Resource instrumentsResource;

    @Value("classpath:/reference-data/patterns.json")
    private Resource patternsResource;

    @Bean
    public ReferenceDataService referenceDataService() throws IOException {
        return ReferenceDataService.builder()
                .colors(loadColors())
                .instruments(loadInstruments())
                .patterns(loadPatterns())
                .build();
    }

    private List<Color> loadColors() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream in = colorsResource.getInputStream()) {
            return objectMapper.readValue(in, new TypeReference<List<Color>>() {
            });
        }
    }

    private List<Instrument> loadInstruments() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream in = instrumentsResource.getInputStream()) {
            return objectMapper.readValue(in, new TypeReference<List<Instrument>>() {
            });
        }
    }

    private List<Pattern> loadPatterns() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream in = patternsResource.getInputStream()) {
            return objectMapper.readValue(in, new TypeReference<List<Pattern>>() {
            });
        }
    }

}
