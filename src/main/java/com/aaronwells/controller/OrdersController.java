package com.aaronwells.controller;

import com.aaronwells.model.Order;
import com.aaronwells.service.orders.OrdersSheetService;
import com.aaronwells.service.paypal.PaypalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = "*")
public class OrdersController {

    private final PaypalService paypalService;
    private final OrdersSheetService ordersSheetService;

    public OrdersController(PaypalService paypalService, OrdersSheetService ordersSheetService) {
        this.paypalService = paypalService;
        this.ordersSheetService = ordersSheetService;
    }

    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<List<Object>>> recordPaypalTransaction(@RequestBody Order order) throws IOException {
        log.debug("POST /api/orders");
        var paypalDetails = paypalService.fetchDetails(order.getPaypalId());
        if (paypalDetails == null)
            return new ResponseEntity<>(HttpStatus.OK);

        log.debug("Calling orderSheetsService.saveOrderDetails()");
        ordersSheetService.saveOrderDetails(paypalDetails, order);
        log.debug("Done calling ordersSheetService.saveOrderDetails()");

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
