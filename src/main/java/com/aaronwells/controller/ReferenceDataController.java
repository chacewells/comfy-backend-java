package com.aaronwells.controller;

import com.aaronwells.model.Color;
import com.aaronwells.model.Instrument;
import com.aaronwells.model.Pattern;
import com.aaronwells.service.ReferenceDataService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class ReferenceDataController {
    private final ReferenceDataService referenceDataService;

    public ReferenceDataController(ReferenceDataService referenceDataService) {
        this.referenceDataService = referenceDataService;
    }

    @GetMapping(value = "/colors", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Color> getColors() {
        return referenceDataService.getColors();
    }

    @GetMapping(value = "/instruments", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Instrument> getInstruments() {
        return referenceDataService.getInstruments();
    }

    @GetMapping(value = "/patterns", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Pattern> getPatterns() {
        return referenceDataService.getPatterns();
    }
}
