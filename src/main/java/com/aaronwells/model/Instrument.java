package com.aaronwells.model;

import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
public class Instrument extends BaseAttribute {
    @Builder
    public Instrument(UUID id, String name) {
        super(id, name);
    }
}
