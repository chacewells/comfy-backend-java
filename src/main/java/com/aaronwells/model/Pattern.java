package com.aaronwells.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@NoArgsConstructor
public class Pattern extends BaseAttribute {

    @JsonProperty
    private BigDecimal price;

    @JsonProperty
    private String image;

    @JsonProperty("can_embroider")
    private Boolean canEmbroider;

    @Builder
    public Pattern(UUID id, String name, BigDecimal price, String image, Boolean canEmbroider) {
        super(id, name);
        this.price = price;
        this.image = image;
        this.canEmbroider = canEmbroider;
    }

}
