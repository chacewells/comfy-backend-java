package com.aaronwells.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Strap {

    @JsonProperty("_id")
    private UUID id;

    @JsonProperty
    private Pattern pattern;

    @JsonProperty
    private Instrument instrument;

    @JsonProperty
    private Boolean embroidered;

    @JsonProperty
    private Color embroideredColor;

    @JsonProperty
    private String embroideredText;

    @JsonProperty
    private String notes;

    @JsonProperty
    private Integer quantity;

}
