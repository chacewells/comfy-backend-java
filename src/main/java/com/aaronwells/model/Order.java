package com.aaronwells.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Order {

    @JsonProperty("_id")
    private UUID id;

    @JsonProperty
    private List<Strap> cart;

    @JsonProperty("paypal_id")
    private String paypalId;

}
