package com.aaronwells.model;

import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
public class Color extends BaseAttribute {
    @Builder
    public Color(UUID id, String name) {
        super(id, name);
    }
}
