package com.aaronwells.model;

import lombok.Builder;
import lombok.Getter;

import java.util.Objects;

@Builder
@Getter
public class PaypalDetails {

    private String paypalId;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String fullName;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;
        if ( !(obj instanceof PaypalDetails) ) return false;

        PaypalDetails other = (PaypalDetails) obj;

        return Objects.equals(paypalId, other.paypalId)
            && Objects.equals(addressLine1, other.addressLine1)
            && Objects.equals(addressLine2, other.addressLine2)
            && Objects.equals(city, other.city)
            && Objects.equals(state, other.state)
            && Objects.equals(country, other.country)
            && Objects.equals(postalCode, other.postalCode)
            && Objects.equals(fullName, other.fullName);
    }
}
