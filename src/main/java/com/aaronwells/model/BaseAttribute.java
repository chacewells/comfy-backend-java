package com.aaronwells.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseAttribute {

    @JsonProperty( "_id" )
    private UUID id;

    @JsonProperty
    private String name;

}
