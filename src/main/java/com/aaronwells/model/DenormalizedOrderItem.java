package com.aaronwells.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class DenormalizedOrderItem {
    private String paypalId;
    private String customerFullName;
    private String customerAddressLine1;
    private String customerAddressLine2;
    private String customerCity;
    private String customerState;
    private String customerCountry;
    private String customerPostalCode;
    private String patternName;
    private String instrumentName;
    private Boolean embroidered;
    private String embroideredColorName;
    private String embroideredText;
    private String notes;
    private String quantity;
}
